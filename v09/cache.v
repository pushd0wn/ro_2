`timescale 1ns / 1ps

module cache
#(
    parameter LOG_SIZE = 6
)
(
    input clk,
    input res,
    // Schnittstelle zwischen Prozessor und Cache
    input         cached_instr_req,
    input [31:0]  cached_instr_adr,
    output reg       cached_instr_gnt,
    output reg       cached_instr_rvalid,
    output [31:0] cached_instr_read,
    // Schnittstelle zwischen Cache und Hauptspeicher
    output reg       instr_req,
    output [31:0] instr_adr,
    input         instr_gnt,
    input         instr_rvalid,
    input [31:0]  instr_read
);
    reg [31:0]          lines   [(2**LOG_SIZE) - 1:0];
    reg [29-LOG_SIZE:0] tags    [(2**LOG_SIZE) - 1:0];
    reg [(2**LOG_SIZE) - 1:0] valids;
    wire                hit;
    wire [LOG_SIZE-1:0]   index;

    assign index = cached_instr_adr [LOG_SIZE + 1: 2];
    assign hit = (cached_instr_adr[31: LOG_SIZE + 2] == tags[index]) && valids[index];
    assign instr_adr[31:0] = cached_instr_adr;
    assign cached_instr_read[31:0] = lines[index];

    always @(posedge clk) begin
        if(res == 1'b1)
            valids <= {2**LOG_SIZE{1'b0}};
        else
            if (hit == 0 && instr_rvalid == 1)
            begin
                //Update on Cache miss
                lines   [index] <= instr_read[31:0];
                tags    [index] <= cached_instr_adr[31:LOG_SIZE+2];
                valids  [index] <= 1'b1;
            end
    end

    //Mealy State machine
    localparam STATES = 5;
    localparam IDLE = 5'd1, HIT = 5'd2, RESULT = 5'd3, MISS  = 5'd4, READING = 5'd5;

    reg [2:0] state, next_state;

    always @(state, cached_instr_req, hit, instr_gnt, instr_rvalid, instr_read, index)
    begin 
        case(state)
            IDLE: begin
                cached_instr_rvalid <= 0;
                if (cached_instr_req == 1 && hit == 1) begin
                    cached_instr_gnt <= 1;
                    next_state <= HIT;
                    instr_req <= 0;
                end
                else if (cached_instr_req == 1 && hit == 0) begin
                    cached_instr_gnt <= 0;
                    instr_req <= 0;
                    next_state <= MISS;
                end
                else begin
                    cached_instr_gnt <= 0;
                    instr_req <= 0;
                    next_state <= IDLE;
                end
            end
            HIT: begin
                next_state <= RESULT;
                cached_instr_gnt <= 0;
                instr_req <= 0;
                cached_instr_rvalid <= 1;
            end
            RESULT: begin
                next_state <= IDLE;
                cached_instr_gnt <= 0;
                instr_req <= 0;
                cached_instr_rvalid <= 1;
            end
            MISS: begin
                instr_req <= 1;
                if(instr_rvalid == 1) begin
                    next_state <= RESULT;
                    cached_instr_gnt <= 1;
                    cached_instr_rvalid <= 0;
                end
                else if (instr_gnt == 1) begin
                    next_state <= READING;
                    cached_instr_gnt <= 1;
                    cached_instr_rvalid <= 0;
                end
                else begin
                    next_state <= MISS;
                    cached_instr_gnt <= 1;
                    cached_instr_rvalid <= 0;
                end 
            end
            READING: begin
                instr_req <= 0;
                cached_instr_gnt <= 0;
                if (instr_rvalid == 1) begin
                    next_state <= RESULT;
                    cached_instr_rvalid <= 0;
                end
                else begin
                    next_state <= READING;
                    cached_instr_rvalid <= 0;
                end
            end
            default: begin
                next_state <= IDLE;
                cached_instr_gnt <= 0;
                cached_instr_rvalid <= 0;
                instr_req <= 0;
            end
        endcase
    end

    always @(posedge clk)
    begin
        if (res==1'b1)  state <= IDLE;
        else            state <= next_state;
    end
endmodule