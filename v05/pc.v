module pc(
    input [31:0] D,
    input MODE, ENABLE, RES, CLK,
    output reg [31:0] PC_OUT
    );
    always @(posedge CLK)
    begin
    if(RES==1)
        PC_OUT <= 32'h1A00_0000;
    else if(ENABLE==1)
        if(MODE==1)
            PC_OUT <= D;
        else
            PC_OUT <= PC_OUT + 4;
    end
endmodule