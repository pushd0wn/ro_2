`define REG_BIT_WIDTH 32

module regset_tb();
    reg [`REG_BIT_WIDTH-1:0] D;
    reg [4:0] A_D, A_Q0, A_Q1;
    reg write_enable, RES;
    reg CLK;
    reg [`REG_BIT_WIDTH-1:0] soll;
    reg [4:0] addr;
    wire [31:0] Q0, Q1;

    regset dut(.D(D), .A_D(A_D), .A_Q0(A_Q0), .A_Q1(A_Q1), .write_enable(write_enable), .RES(RES), .CLK(CLK), .Q0(Q0), .Q1(Q1));

    integer i;

    initial begin
        CLK=0;
        RES=0;
        //seed random
        $srandom(42);

        //Write Test
        soll=$urandom;
        addr=$urandom_range(15,1);
        A_D=addr;
        D=soll;
        write_enable=1;
        #10;
        A_Q0 = addr;
        #5;
        if(Q0 != soll)
        begin
            $display("Write fehlerhaft");
            $finish;
        end
        
        A_D=$urandom_range(31,16);
        D=$urandom;
        A_Q1 = addr;
        A_Q0 = A_D;
        #10;
        if(Q1 != soll || Q0 != D)
        begin
            $display("Write fehlerhaft");
            $finish;
        end
        
        // test: overwriting 0-register
        A_D=32'd0;
        D=32'd1;
        A_Q0 = A_D;
        #10;
        if(Q0 != 32'd0)
        begin
            $display("Nulltes Register inkorrekt");
            $finish;
        end


        write_enable=0;
        soll=$urandom;
        D=soll;
        #10;
        if(Q0 == soll)
        begin
            $display("Write Enable fehlerhaft");
            $finish;
        end

        //filling with random values
        write_enable=1;
        for (i=0; i<32; i=i+1)
        begin
            A_Q0 = i;
            A_D = i;
            @ (negedge CLK)
            begin
                D=$urandom;
            end
        end
        write_enable=0;
        #10;
        //checking reset
        CLK=0;
        RES=1;
        @ (negedge CLK);
        RES=0;
        for (i=0; i<32; i=i+1)
        begin
            A_Q0 = i;
            A_Q1 = i;
            @ (negedge CLK)
            if(Q0 !=32'd0 || Q1!=32'd0)
            begin
                $display("Reset fehlerhaft");
                $finish;
            end
        end

        //checking reset dominant over write_enable
        @( negedge CLK);
        RES=1;
        write_enable=1;
        A_D=1;
        soll=$urandom;
        A_Q0=1;
        A_Q1=1;
        @( negedge CLK);
        RES=0;
        //write_enable=0;
        if(Q0 == soll || Q1 == soll)
        begin
            begin
                $display("Reset/Write fehlerhaft");
                $finish;
            end
        end
    end
    always
        #5 CLK=~CLK;
    initial
        #10000 $finish;
    /*initial begin
        $dumpfile("sim.vcd");
        $dumpvars(0, regset_tb);
    end*/
endmodule