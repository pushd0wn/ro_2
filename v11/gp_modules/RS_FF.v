module RS_FF(
    input R, S, CLK,
    output reg Q
);
    always @(posedge CLK)
    begin
        if (R==1)
        begin
            Q = 0;
        end
        else if (S==1)
        begin
            Q = 1;
        end
    end
endmodule