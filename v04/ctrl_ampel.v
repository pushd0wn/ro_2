`define red 3'b100
`define yellow 3'b110
`define green 3'b010

`define s0 3'b000
`define s1 3'b001
`define s2 3'b010
`define s3 3'b011
`define s4 3'b100

module ctrl_ampel(
    input BTN, TICK, RES, CLK,
    output reg [2:0] RGB,
    output reg CNTR_RES
    );
    reg [2:0] state;
    always @(posedge CLK, posedge RES)
    begin
        if(RES)
        begin
            state <= `s0;            
        end
        else
        begin
            if(state == `s0 && BTN == 1)
                state <= `s1;
            else if (state == `s1 && TICK == 1)
                state <= `s2;
            else if (state == `s2 && TICK == 1)
                state <= `s3;
            else if (state == `s3 && TICK == 1)
                state <= `s4;
            else if (state == `s4 && TICK == 1)
                state <= `s0;
        end
    end
    always @(state)
    begin
        case(state)
            `s0: begin
                RGB <= `red;
                CNTR_RES <= 1;
            end
            `s1: begin
                RGB <= `red;
                CNTR_RES <= 0;
            end
            `s2: RGB <= `yellow;
            `s3: RGB <= `green;
            `s4: RGB <= `yellow;
        endcase
    end
endmodule
