module ampel(
    input BTN, RESN, CLK,
    output wire [2:0] RGB
    );
    wire RES = ~RESN;
    wire TICK, CNTR_RES;
    tc timer(.RES(CNTR_RES), .CLK(CLK), .TICK(TICK));
    ctrl_ampel controller(.BTN(BTN), .TICK(TICK), .RES(RES), .CLK(CLK), .RGB(RGB), .CNTR_RES(CNTR_RES));
endmodule
