`timescale 1ns / 1ps

module system(
    input BOARD_CLK, BOARD_RESN, BOARD_UART_RX,
    input [3:0] BOARD_BUTTON, BOARD_SWITCH,
    output BOARD_VGA_HSYNC, BOARD_VGA_VSYNC, BOARD_UART_TX,
    output [2:0] BOARD_LED_RGB0, BOARD_LED_RGB1,
    output [3:0] BOARD_LED, BOARD_VGA_R, BOARD_VGA_G, BOARD_VGA_B
    );
    //CORE SIGNALS
    wire CPU_CLK, CPU_RES, CACHE_RES;
    //Instruction memory interface
    wire instr_req, instr_gnt, instr_r_valid;
    wire [31:0] instr_read, instr_adr;
    //Instruction caches
    wire cached_instr_req_a, cached_instr_gnt_a, cached_instr_r_valid_a;
    wire [31:0] cached_instr_read_a, cached_instr_adr_a;
    wire cached_instr_req_b, cached_instr_gnt_b, cached_instr_r_valid_b;
    wire [31:0] cached_instr_read_b, cached_instr_adr_b;
    //Data memory interface
    wire data_req, data_gnt, data_r_valid, data_write_enable;
    wire [3:0] data_be;
    wire [31:0] data_read, data_adr, data_write;
    //Interrupts
    wire irq, irq_ack;
    wire [4:0] irq_id, irq_ack_id;
    //MemHub
    wire [31:0] instr_read_hub;
    wire instr_gnt_a, instr_r_valid_a, instr_req_a;
    wire instr_gnt_b, instr_r_valid_b, instr_req_b;
    wire [31:0] data_read_hub;
    wire [31:0] data_write_a, data_adr_a, instr_adr_a;
    wire [31:0] data_write_b, data_adr_b, instr_adr_b;
    wire data_gnt_a, data_r_valid_a, data_write_enable_a, data_req_a;
    wire data_gnt_b, data_r_valid_b, data_write_enable_b, data_req_b;
    wire [3:0] data_be_a, data_be_b;
    //Clock
    reg clk;

    pulpus pulpus(.BOARD_CLK(clk),
        .BOARD_RESN(BOARD_RESN),
        .BOARD_LED(BOARD_LED),
        .BOARD_LED_RGB0(BOARD_LED_RGB0),
        .BOARD_LED_RGB1(BOARD_LED_RGB1),
        .BOARD_BUTTON(BOARD_BUTTON),
        .BOARD_SWITCH(BOARD_SWITCH),
        .BOARD_VGA_HSYNC(BOARD_VGA_HSYNC),
        .BOARD_VGA_VSYNC(BOARD_VGA_VSYNC),
        .BOARD_VGA_R(BOARD_VGA_R),
        .BOARD_VGA_G(BOARD_VGA_G),
        .BOARD_VGA_B(BOARD_VGA_B),
        .BOARD_UART_RX(BOARD_UART_RX),
        .BOARD_UART_TX(BOARD_UART_TX),
        .CPU_CLK(CPU_CLK),
        .CPU_RES(CPU_RES),
        .CACHE_RES(CACHE_RES),
        .INSTR_REQ(instr_req),
        .INSTR_GNT(instr_gnt),
        .INSTR_RVALID(instr_r_valid),
        .INSTR_ADDR(instr_adr),
        .INSTR_RDATA(instr_read),
        .DATA_REQ(data_req),
        .DATA_GNT(data_gnt),
        .DATA_RVALID(data_r_valid),
        .DATA_WE(data_write_enable),
        .DATA_BE(data_be),
        .DATA_ADDR(data_adr),
        .DATA_WDATA(data_write),
        .DATA_RDATA(data_read),
        .IRQ(irq),
        .IRQ_ID(irq_id),
        .IRQ_ACK(irq_ack),
        .IRQ_ACK_ID(irq_ack_id));

    proc #(.MHARTID(0))
        cpu_a(.clk(CPU_CLK), .res(CPU_RES),
        .instr_read(cached_instr_read_a),
        .instr_adr(cached_instr_adr_a),
        .instr_req(cached_instr_req_a),
        .instr_gnt(cached_instr_gnt_a),
        .instr_r_valid(cached_instr_r_valid_a),
        .data_read(data_read_hub),
        .data_write(data_write_a),
        .data_adr(data_adr_a),
        .data_req(data_req_a),
        .data_gnt(data_gnt_a),
        .data_r_valid(data_r_valid_a),
        .data_write_enable(data_write_enable_a),
        .data_be(data_be_a),
        .irq(irq),
        .irq_id(irq_id),
        .irq_ack(irq_ack),
        .irq_ack_id(irq_ack_id));

    proc #(.MHARTID(1))
        cpu_b(.clk(CPU_CLK), .res(CPU_RES),
        .instr_read(cached_instr_read_b),
        .instr_adr(cached_instr_adr_b),
        .instr_req(cached_instr_req_b),
        .instr_gnt(cached_instr_gnt_b),
        .instr_r_valid(cached_instr_r_valid_b),
        .data_read(data_read_hub),
        .data_write(data_write_b),
        .data_adr(data_adr_b),
        .data_req(data_req_b),
        .data_gnt(data_gnt_b),
        .data_r_valid(data_r_valid_b),
        .data_write_enable(data_write_enable_b),
        .data_be(data_be_b),
        .irq(0),
        .irq_id(0),
        .irq_ack(),
        .irq_ack_id());

    cache cache_a(.clk(CPU_CLK), .res(CPU_RES),
        .cached_instr_req(cached_instr_req_a),
        .cached_instr_adr(cached_instr_adr_a),
        .cached_instr_gnt(cached_instr_gnt_a),
        .cached_instr_rvalid(cached_instr_r_valid_a),
        .cached_instr_read(cached_instr_read_a),
        .instr_req(instr_req_a),
        .instr_adr(instr_adr_a),
        .instr_gnt(instr_gnt_a),
        .instr_rvalid(instr_r_valid_a),
        .instr_read(instr_read_hub));
        
    cache cache_b(.clk(CPU_CLK), .res(CPU_RES),
        .cached_instr_req(cached_instr_req_b),
        .cached_instr_adr(cached_instr_adr_b),
        .cached_instr_gnt(cached_instr_gnt_b),
        .cached_instr_rvalid(cached_instr_r_valid_b),
        .cached_instr_read(cached_instr_read_b),
        .instr_req(instr_req_b),
        .instr_adr(instr_adr_b),
        .instr_gnt(instr_gnt_b),
        .instr_rvalid(instr_r_valid_b),
        .instr_read(instr_read_hub));

    mealy_mem_hub instr_hub(.clk(CPU_CLK), .res(CPU_RES),
        .write_cpu_a(0),
        .write_cpu_b(0),
        .read_mem(instr_read),
        .adr_a(instr_adr_a),
        .adr_b(instr_adr_b),
        .req_a(instr_req_a),
        .req_b(instr_req_b),
        .gnt_mem(instr_gnt),
        .r_valid_mem(instr_r_valid),
        .write_enable_a(0),
        .write_enable_b(0),
        .be_a(0),
        .be_b(0),
        .write_mem(),
        .adr_mem(instr_adr),
        .read_cpu(instr_read_hub),
        .be_mem(),
        .req_mem(instr_req),
        .gnt_a(instr_gnt_a),
        .gnt_b(instr_gnt_b),
        .r_valid_a(instr_r_valid_a),
        .r_valid_b(instr_r_valid_b),
        .write_enable_mem());
    

    mealy_mem_hub data_hub(.clk(CPU_CLK), .res(CPU_RES),
        .write_cpu_a(data_write_a),
        .write_cpu_b(data_write_b),
        .read_mem(data_read),
        .adr_a(data_adr_a),
        .adr_b(data_adr_b),
        .req_a(data_req_a),
        .req_b(data_req_b),
        .gnt_mem(data_gnt),
        .r_valid_mem(data_r_valid),
        .write_enable_a(data_write_enable_a),
        .write_enable_b(data_write_enable_b),
        .be_a(data_be_a),
        .be_b(data_be_b),
        .write_mem(data_write),
        .adr_mem(data_adr),
        .read_cpu(data_read_hub),
        .be_mem(data_be),
        .req_mem(data_req),
        .gnt_a(data_gnt_a),
        .gnt_b(data_gnt_b),
        .r_valid_a(data_r_valid_a),
        .r_valid_b(data_r_valid_b),
        .write_enable_mem(data_write_enable));
        
    `ifdef XILINX_SIMULATOR
        //Vivado Simulator specific code
        initial
            begin
                clk=0;
            end
        always
            #5 clk = ~clk;
    `else
        always @(BOARD_CLK)
            clk= BOARD_CLK;
    `endif
    
endmodule