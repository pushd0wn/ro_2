#Enable Timer Interrupts
addi a0, x0, 0b10000000000
lui a1, 0x1A109
sw a0, 0x004(a1)
#Set Timer Delay
addi a0, x0, 50
lui a1, 0x1A10B
sw a0, 0x010(a1)
#Set Timer CFG (CMP_CLR, IRQ, RESET, ENABLE)
addi a0, x0, 0b010111
sw a0, 0x0(a1)
#Loop until Interrupted
addi a2, x0, 0
addi a3, x0, 10
loop:
	nop
    blt a2, a3, loop
#Turn off Timer
addi a0, x0, 0
sw a0, 0x0(a1)
