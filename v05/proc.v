`timescale 1ns / 1ps

module proc(
    input clk, res,
    //Instruction memory interface
    input [31:0] instr_read,
    output [31:0] instr_adr,
    output instr_req,
    input instr_gnt, instr_r_valid,
    //Data interface
    input [31:0] data_read,
    output [31:0] data_write, data_adr,
    output data_req,
    input data_gnt, data_r_valid,
    output data_write_enable,
    output [3:0] data_be,
    //Interrupts
    input irq,
    input [4:0] irq_id,
    output irq_ack,
    output [4:0] irq_ack_id
    );
    wire pc_enable, instr_reg_enable, regset_write_enable, pc_mode, regset_write_logic, alu_cmp;
    wire [31:0] regset_q0, regset_q1, alu_b, alu_out, imm_load, imm_upper_mux_out, instruction, pc_data, pc_out, result, imm_gen_out;
    wire [5:0] alu_op;
    ctrl controller(.clk(clk),
        .res(res),
        .instr_gnt(instr_gnt),
        .instr_r_valid(instr_r_valid),
        .instr_req(instr_req),
        .pc_enable(pc_enable),
        .instr_reg_enable(instr_reg_enable),
        .regset_write_enable(regset_write_enable));
    assign instr_adr[31:0] = pc_out;
    pc pc(.D(pc_data),
        .MODE(pc_mode),
        .ENABLE(pc_enable),
        .RES(res),
        .CLK(clk),
        .PC_OUT(pc_out));
    REG_DRE_32 instr_reg(.D(instr_read),
        .Q(instruction),
        .CLK(clk),
        .RES(res),
        .ENABLE(instr_reg_enable));
    assign regset_write_logic = instruction[4] & regset_write_enable;
    regset regset(.D(result),
        .A_D(instruction[11:7]),
        .A_Q0(instruction[19:15]),
        .A_Q1(instruction[24:20]),
        .write_enable(regset_write_logic),
        .RES(res),
        .CLK(clk),
        .Q0(regset_q0),
        .Q1(regset_q1));
    assign alu_op[5] = instruction[30];
    assign alu_op[4:2] = instruction[14:12];
    assign alu_op[1:0] = instruction[6:5];
    MUX_2x1_32 alu_b_mux(.I0(imm_gen_out),
        .I1(regset_q1),
        .S(instruction[5]),
        .Y(alu_b));
    alu alu(.S(alu_op),
        .A(regset_q0),
        .B(alu_b),
        .CMP(alu_cmp),
        .Q(alu_out));
    MUX_2x1_32 alu_out_mux(.I0(alu_out),
        .I1(imm_load),
        .S(instruction[2]),
        .Y(result));
    assign pc_mode = instruction[6] & alu_cmp; 
    imm_gen imm_gen(.in(instruction[31:0]), .out(imm_gen_out));
    assign pc_data = (imm_gen_out<<1) + pc_out;
    MUX_2x1_32 imm_upper_mux(.I0(pc_out),
        .I1(32'd0),
        .S(instruction[5]),
        .Y(imm_upper_mux_out));
    assign imm_load = imm_upper_mux_out + imm_gen_out;// << 12);
endmodule
