module pc_tb();
    reg [31:0] D;
    reg MODE, ENABLE, RES, CLK;
    wire [31:0] PC_OUT;

    pc dut(.D(D), .ENABLE(ENABLE), .MODE(MODE), .RES(RES), .CLK(CLK), .PC_OUT(PC_OUT));

    reg [31:0] soll;

    initial begin
        CLK=0;
        RES=0;
        //seed random
        $srandom(42);


        //Load test
        ENABLE=1;
        MODE=1;
        soll=$urandom;
        D=soll;
        @(negedge CLK);
        MODE=0;
        if(PC_OUT != soll)
        begin
            $display("Load fehlerhaft");
            $finish;
        end
        //inc test
        @(negedge CLK);
        if(PC_OUT != (soll + 4))
        begin
            $display("Inkrement fehlerhaft");
            $finish;
        end
        ENABLE=0;
        //not enabled test
        @(negedge CLK);
        if(PC_OUT != (soll + 4))
        begin
            $display("Enable fehlerhaft");
            $finish;
        end
        ENABLE=1;
        RES=1;
        @(negedge CLK);
        //reset (and reset dominant) test
        if(PC_OUT != 32'h1A00_0000)
        begin
            $display("Reset fehlerhaft");
            $finish;
        end
         
    end
    always
        #5 CLK=~CLK;
    initial
        #1000 $finish;
    /*initial begin
        $dumpfile("sim.vcd");
        $dumpvars(0, pc_tb);
    end*/
endmodule