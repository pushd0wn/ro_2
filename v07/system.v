`timescale 1ns / 1ps

module system(
    input BOARD_CLK, BOARD_RESN, BOARD_UART_RX,
    input [3:0] BOARD_BUTTON, BOARD_SWITCH,
    output BOARD_VGA_HSYNC, BOARD_VGA_VSYNC, BOARD_UART_TX,
    output [2:0] BOARD_LED_RGB0, BOARD_LED_RGB1,
    output [3:0] BOARD_LED, BOARD_VGA_R, BOARD_VGA_G, BOARD_VGA_B
    );
    //CORE SIGNALS
    wire CPU_CLK, CPU_RES, CACHE_RES;
    //Instruction memory interface
    wire instr_req, instr_gnt, instr_r_valid;
    wire [31:0] instr_read, instr_adr;
    //Instruction cache
    wire cached_instr_req, cached_instr_gnt, cached_instr_r_valid;
    wire [31:0] cached_instr_read, cached_instr_adr;
    //Data memory interface
    wire data_req, data_gnt, data_r_valid, data_write_enable;
    wire [3:0] data_be;
    wire [31:0] data_read, data_adr, data_write;
    //Interrupts
    wire irq, irq_ack;
    wire [4:0] irq_id, irq_ack_id;
    //Clock
    reg clk;

    pulpus pulpus(.BOARD_CLK(clk),
        .BOARD_RESN(BOARD_RESN),
        .BOARD_LED(BOARD_LED),
        .BOARD_LED_RGB0(BOARD_LED_RGB0),
        .BOARD_LED_RGB1(BOARD_LED_RGB1),
        .BOARD_BUTTON(BOARD_BUTTON),
        .BOARD_SWITCH(BOARD_SWITCH),
        .BOARD_VGA_HSYNC(BOARD_VGA_HSYNC),
        .BOARD_VGA_VSYNC(BOARD_VGA_VSYNC),
        .BOARD_VGA_R(BOARD_VGA_R),
        .BOARD_VGA_G(BOARD_VGA_G),
        .BOARD_VGA_B(BOARD_VGA_B),
        .BOARD_UART_RX(BOARD_UART_RX),
        .BOARD_UART_TX(BOARD_UART_TX),
        .CPU_CLK(CPU_CLK),
        .CPU_RES(CPU_RES),
        .CACHE_RES(CACHE_RES),
        .INSTR_REQ(instr_req),
        .INSTR_GNT(instr_gnt),
        .INSTR_RVALID(instr_r_valid),
        .INSTR_ADDR(instr_adr),
        .INSTR_RDATA(instr_read),
        .DATA_REQ(data_req),
        .DATA_GNT(data_gnt),
        .DATA_RVALID(data_r_valid),
        .DATA_WE(data_write_enable),
        .DATA_BE(data_be),
        .DATA_ADDR(data_adr),
        .DATA_WDATA(data_write),
        .DATA_RDATA(data_read),
        .IRQ(irq),
        .IRQ_ID(irq_id),
        .IRQ_ACK(irq_ack),
        .IRQ_ACK_ID(irq_ack_id));

    proc cpu(.clk(CPU_CLK), .res(CPU_RES),
        .instr_read(cached_instr_read),
        .instr_adr(cached_instr_adr),
        .instr_req(cached_instr_req),
        .instr_gnt(cached_instr_gnt),
        .instr_r_valid(cached_instr_r_valid),
        .data_read(data_read),
        .data_write(data_write),
        .data_adr(data_adr),
        .data_req(data_req),
        .data_gnt(data_gnt),
        .data_r_valid(data_r_valid),
        .data_write_enable(data_write_enable),
        .data_be(data_be),
        .irq(irq),
        .irq_id(irq_id),
        .irq_ack(irq_ack),
        .irq_ack_id(irq_ack_id));
        
     cache cache(.clk(CPU_CLK), .res(CPU_RES),
        .cached_instr_req(cached_instr_req),
        .cached_instr_adr(cached_instr_adr),
        .cached_instr_gnt(cached_instr_gnt),
        .cached_instr_rvalid(cached_instr_r_valid),
        .cached_instr_read(cached_instr_read),
        .instr_req(instr_req),
        .instr_adr(instr_adr),
        .instr_gnt(instr_gnt),
        .instr_rvalid(instr_r_valid),
        .instr_read(instr_read));
        
       
        
    `ifdef XILINX_SIMULATOR
        //Vivado Simulator specific code
        initial
            begin
                clk=0;
            end
        always
            #5 clk = ~clk;
    `else
        always @(BOARD_CLK)
            clk= BOARD_CLK;
    `endif
    
endmodule