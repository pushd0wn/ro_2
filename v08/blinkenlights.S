nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
jal x0, BUTTON
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop

#Enable Button Interrupts
addi a0, x0, 0b1001
sw a0 0(a1)
lui a0, 0b1000
lui a1, 0x1A109
lw t0, 0(a1)
or a0, a0, t0
sw a0, 0x004(a1)
jalr x0, ra, 0

# Button Interrupt
BUTTON:
#INT_STATUS
lui t0, 0x1A101
lw t1 0x018(t0)
srli t2, t1, 15
andi t2, t2, 0b1111
#Lights address
lui a1, 0x1A120
addi a0, x0, 0b1111
sw a0 0(a1)

#Testing which light pattern is selected
addi a0, x0, 8
bge t2, a0, TASTER_0
srli a0, a0, 1
bge t2, a0, TASTER_1
srli a0, a0, 1
bge t2, a0, TASTER_2
srli a0, a0, 1
bge t2, a0, TASTER_3
mret

TASTER_0:
addi a0, x0, 0b1111
sw a0 0(a1)
mret
TASTER_1:
addi a0, x0, 0b1100
sw a0 0(a1)
mret
TASTER_2:
addi a0, x0, 0b0011
sw a0 0(a1)
mret
TASTER_3:
addi a0, x0, 0b1001
sw a0 0(a1)
mret
