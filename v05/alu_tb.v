
module alu_tb();
  
  reg [31:0] A, B;
  reg [5:0] S;
  wire [31:0] Q;
  wire CMP;
  
  alu dut(.A(A), .B(B), .S(S), .Q(Q), .CMP(CMP));
  
  initial begin
  
    S = 6'b100011; A = 0; B = 0; #10;
    if (CMP != 1)
      $finish;
  
    S = 6'b000011; A = 1; B = 0; #10;
    if (CMP != 0)
      $finish;

    S = 6'b100111; A = 1; B = 0; #10;
    if (CMP != 1)
      $finish;

    S = 6'b000111; A = 0; B = 0; #10;
    if (CMP != 0)
      $finish;
      
    S = 6'b110011; A = 7; B = 42; #10;
    if (CMP != 1)
      $finish;

    S = 6'b110011; A = 9; B = 9; #10;
    if (CMP != 0)
      $finish;

    S = 6'b010011; A = -1; B = 1; #10;
    if (CMP != 1)
      $finish;
      
    S = 6'b110111; A = 112; B = 4; #10;
    if (CMP != 1)
      $finish;

    S = 6'b110111; A = 7; B = 7; #10;
    if (CMP != 1)
      $finish;
      
    S = 6'b010111; A = -100000; B = 65000; #10;
    if (CMP != 0)
      $finish;

    S = 6'b011011; A = 3; B = 22; #10;
    if (CMP != 1)
      $finish;

    S = 6'b111011; A = 1 << 31; B = 0; #10;
    if (CMP != 0)
      $finish;

    S = 6'b011111; A = 12; B = 12; #10;
    if (CMP != 1)
      $finish;

    S = 6'b111111; A = 10; B = 1 << 31; #10;
    if (CMP != 0)
      $finish;
      
    S = 6'b100000; A = 5; B = 7; #10;
    if (Q != 12)
      $finish;
    
    S = 6'b000001; A = -1; B = 1; #10;
    if (Q != 0)
      $finish;
    
    S = 6'b100001; A = -3; B = -5; #10;
    if (Q != 2)
      $finish;
        
    S = 6'b100001; A = 18; B = 403; #10;
    if (Q != -385)
      $finish;
      
    S = 6'b001001; A = 8; B = 43; #10;
    if (Q != 1)
      $finish;
    
    S = 6'b101000; A = 10; B = 10; #10;
    if (Q != 0)
      $finish;

    S = 6'b101000; A = -2; B = -1; #10;
    if (Q != 1)
      $finish;
      
    S = 6'b001101; A = 12; B = 13; #10;
    if (Q != 1)
      $finish;

    S = 6'b101101; A = -38; B = 5; #10;
    if (Q != 0)
      $finish;
      
    S = 6'b101100; A = 21; B = 21; #10;
    if (Q != 0)
      $finish;

    S = 6'b001101; A = 5; B = -2; #10;
    if (Q != 1)
      $finish;
      
    S = 6'b010001; A = 42; B = 32'h7f; #10;
    if (Q != 85)
      $finish;

    S = 6'b110000; A = 42; B = 85; #10;
    if (Q != 32'h7f)
      $finish;
      
    S = 6'b010000; A = 53; B = 0; #10;
    if (Q != 53)
      $finish;

    S = 6'b011001; A = 2476; B = 1928; #10;
    if (Q != 4012)
      $finish;
      
    S = 6'b111000; A = 488; B = -1; #10;
    if (Q != -1)
      $finish;

    S = 6'b011000; A = 371; B = 0; #10;
    if (Q != 371)
      $finish;
      
    S = 6'b011101; A = 42; B = 85; #10;
    if (Q != 0)
      $finish;

    S = 6'b111100; A = 315; B = 0; #10;
    if (Q != 0)
      $finish;
      
    S = 6'b011100; A = 731; B = -1; #10;
    if (Q != 731)
      $finish;

    S = 6'b000101; A = -1; B = 1; #10;
    if (Q != -2)
      $finish;
      
    S = 6'b000100; A = 105; B = 3; #10;
    if (Q != 840)
      $finish;

    S = 6'b010101; A = 1060; B = 5; #10;
    if (Q != 33)
      $finish;
      
    S = 6'b010100; A = -1; B = 2; #10;
    if (Q != -1 >> 2)
      $finish;

    S = 6'b110101; A = -2; B = 1; #10;
    if (Q != -1)
      $finish;
      
    S = 6'b110100; A = -1; B = 13; #10;
    if (Q != -1)
      $finish;

  end
endmodule
