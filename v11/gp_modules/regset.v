`define REG_BIT_WIDTH 32

module regset(
    input [`REG_BIT_WIDTH-1:0] D,
    input [4:0] A_D, A_Q0, A_Q1,
    input write_enable, RES, CLK,
    output [`REG_BIT_WIDTH-1:0] Q0, Q1
    );
    reg [`REG_BIT_WIDTH-1:0] register [31:1];
    always @(posedge CLK)
    begin
        if(RES==1) 
            begin: reset
                integer i;
                for (i=1; i<32; i=i+1) 
                    register[i] <= `REG_BIT_WIDTH'd0;
            end 
        else if (write_enable==1)
            register[A_D] <= D;
    end
    assign Q0 = A_Q0 == 0 ? 0: register[A_Q0];
    assign Q1 = A_Q1 == 0 ? 0: register[A_Q1];
endmodule