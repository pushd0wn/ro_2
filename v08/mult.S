# Multiplikation großer Zahlen
# input a1, a2 output a0
add t0, x0, x0
add t1, x0, x0
add t2, x0, x0
add t3, x0, x0
add t4, x0, x0
add t5, x0, x0
add t6, x0, x0
add a0, x0, x0

# variable auf wordbreite setzen
addi t6, x0, 32
# maske auf 1 initialisieren
addi t3, x0, 1
# zählvariable t5 auf 0 initialisieren
add t5, x0, x0

slt t1, a1, x0
beq t1, x0, mult
# a1 positiv machen
addi t4, x0, -1 #32{1'b1}
xor a1, a1, t4
addi a1, a1, 1
mult:
bge t5, t6, endmult
and t2, a1, t3
beq t2, x0, skip
add a0, a0, a2
skip:
addi t5, t5, 1
slli t3, t3, 1
slli a2, a2, 1
jal x0, mult
endmult:
beq t1, x0, end
xor a0, a0, t4
addi a0, a0, 1
end: