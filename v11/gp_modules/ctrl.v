`timescale 1ns / 1ps
`define begin_routine   4'd0
`define write_result    4'd1
`define update_pc       4'd2
`define instr_fetch     4'd3
`define instr_decode    4'd4
`define load_data       4'd5
`define write_data      4'd6
`define mret            4'd7
`define save_pc         4'd8
`define ld_ir_addr      4'd9
`define ir_ack          4'd10
`define multiplication  4'd11

module ctrl(
    input clk, res,
    //Instruction interface
    input instr_gnt, instr_r_valid,
    output reg instr_req, instr_reg_enable, 
    //Data interface
    input data_gnt, data_r_valid, data_access, store, 
    output reg data_req, data_write_enable,
    //Interrupt controller interface 
    input mret, irq, irq_state,
    output reg irq_restore, irq_ack, save_pc, irq_load,
    //Processor control lines
    input mult_active,
    output reg pc_enable, regset_write_enable, rdinstret_inc
    );
    reg [3:0] current_state;
    
    always @(posedge clk, posedge res)
    begin
        if(res==1)
        begin
            current_state <= `instr_fetch;
        end
        else
        begin
            case(current_state)
                `begin_routine: current_state <= 
                        (mret==0) 
                            ? ((data_access==0)
                                    ? ((mult_active == 0) ? `write_result : `multiplication)
                                    : ((store==0) 
                                        ? `load_data 
                                        : `write_data))
                            : (irq==0) 
                                ? `mret 
                                : `ld_ir_addr;
                `load_data: current_state <= (data_r_valid==1) ? `update_pc : `load_data;
                `write_data: current_state <= (data_gnt==1) ? `update_pc : `write_data;
                `write_result: current_state <= `update_pc;
                `update_pc: current_state <= (irq==1 && irq_state==0) ? `save_pc : `instr_fetch;
                `mret: current_state <= `instr_fetch;
                `save_pc: current_state <= `ld_ir_addr;
                `ld_ir_addr: current_state <= `ir_ack;
                `ir_ack: current_state <= `instr_fetch;
                `instr_fetch: current_state <= 
                        (instr_r_valid==1)
                            ? `begin_routine
                            : ((instr_gnt==1) ? `instr_decode : `instr_fetch);
                `instr_decode: current_state <= (instr_r_valid==1) ? `begin_routine : `instr_decode;
                `multiplication: current_state <= `write_result;
                default: current_state <= `instr_fetch;
            endcase
        end
    end
    
    always @(current_state)
    begin
        case(current_state)
            `begin_routine: begin
                regset_write_enable <= 0;
                instr_req <= 0;
                instr_reg_enable <= 1;
                pc_enable <= 0;
                data_req <= 0;
                data_write_enable <= 0;
                save_pc <= 0;
                irq_ack <= 0;
                irq_load <= 0;
                irq_restore <= 0;
                rdinstret_inc <= 0;
            end
            `load_data: begin
                regset_write_enable <= 1;
                instr_req <= 0;
                instr_reg_enable <= 0;
                pc_enable <= 0;
                data_req <= 1;
                data_write_enable <= 0;
                save_pc <= 0;
                irq_ack <= 0;
                irq_load <= 0;
                irq_restore <= 0;
                rdinstret_inc <= 0;
            end
            `write_data: begin
                regset_write_enable <= 0;
                instr_req <= 0;
                instr_reg_enable <= 0;
                pc_enable <= 0;
                data_req <= 1;
                data_write_enable <= 1;
                save_pc <= 0;
                irq_ack <= 0;
                irq_load <= 0;
                irq_restore <= 0;
                rdinstret_inc <= 0;
            end
            `write_result: begin
                regset_write_enable <= 1;
                instr_req <= 0;
                instr_reg_enable <= 0;
                pc_enable <= 0;
                data_req <= 0;
                data_write_enable <= 0;
                save_pc <= 0;
                irq_ack <= 0;
                irq_load <= 0;
                irq_restore <= 0;
                rdinstret_inc <= 0;
            end
            `update_pc: begin
                regset_write_enable <= 0;
                instr_req <= 0;
                instr_reg_enable <= 0;
                pc_enable <= 1;
                data_req <= 0;
                data_write_enable <= 0;
                save_pc <= 0;
                irq_ack <= 0;
                irq_load <= 0;
                irq_restore <= 0;
                rdinstret_inc <= 1;
            end
            `save_pc: begin
                regset_write_enable <= 0;
                instr_req <= 0;
                instr_reg_enable <= 0;
                pc_enable <= 0;
                data_req <= 0;
                data_write_enable <= 0;
                save_pc <= 1;
                irq_ack <= 0;
                irq_load <= 0;
                irq_restore <= 0;
                rdinstret_inc <= 0;
            end
            `ld_ir_addr: begin
                regset_write_enable <= 0;
                instr_req <= 0;
                instr_reg_enable <= 0;
                pc_enable <= 1;
                data_req <= 0;
                data_write_enable <= 0;
                save_pc <= 0;
                irq_ack <= 0;
                irq_load <= 1;
                irq_restore <= 0;
                rdinstret_inc <= 0;
            end
            `ir_ack: begin
                regset_write_enable <= 0;
                instr_req <= 0;
                instr_reg_enable <= 0;
                pc_enable <= 0;
                data_req <= 0;
                data_write_enable <= 0;
                save_pc <= 0;
                irq_ack <= 1;
                irq_load <= 0;
                irq_restore <= 0;
                rdinstret_inc <= 0;
            end
            `mret: begin
                regset_write_enable <= 0;
                instr_req <= 0;
                instr_reg_enable <= 0;
                pc_enable <= 1;
                data_req <= 0;
                data_write_enable <= 0;
                save_pc <= 0;
                irq_ack <= 0;   
                irq_load <= 0;            
                irq_restore <= 1;
                rdinstret_inc <= 0;
            end
            `instr_fetch: begin
                regset_write_enable <= 0;
                instr_req <= 1;
                instr_reg_enable <= 0;
                pc_enable <= 0;
                data_req <= 0;
                data_write_enable <= 0;
                save_pc <= 0;
                irq_ack <= 0;
                irq_load <= 0;
                irq_restore <= 0;
                rdinstret_inc <= 0;
            end
            `instr_decode: begin
                regset_write_enable <= 0;
                instr_req <= 0;
                instr_reg_enable <= 1;
                pc_enable <= 0;
                data_req <= 0;
                data_write_enable <= 0;
                save_pc <= 0;
                irq_ack <= 0;
                irq_load <= 0;
                irq_restore <= 0;
                rdinstret_inc <= 0;
            end
            `multiplication: begin
                regset_write_enable <= 0;
                instr_req <= 0;
                instr_reg_enable <= 0;
                pc_enable <= 0;
                data_req <= 0;
                data_write_enable <= 0;
                save_pc <= 0;
                irq_ack <= 0;
                irq_load <= 0;
                irq_restore <= 0; 
                rdinstret_inc <= 0;
            end
            default: begin
                regset_write_enable <= 0;
                instr_req <= 0;
                instr_reg_enable <= 0;
                pc_enable <= 0;
                data_req <= 0;
                data_write_enable <= 0;
                save_pc <= 0;
                irq_ack <= 0;
                irq_load <= 0;
                irq_restore <= 0; 
                rdinstret_inc <= 0;
            end
        endcase
    end    
endmodule