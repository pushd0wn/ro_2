`timescale 1ns / 1ps
`define RDCYCLE_LS      12'hc00
`define RDCYCLE_MS      12'hc80
`define RDTIME_LS       12'hc01
`define RDTIME_MS       12'hc81
`define RDINSTRET_LS    12'hc02
`define RDINSTRET_MS    12'hc82
`define MHARTID_ADR     12'hf14

module proc
#(
    parameter MHARTID = 0
)
(
    input clk, res,
    //Instruction memory interface
    input [31:0] instr_read,
    output [31:0] instr_adr,
    output instr_req,
    input instr_gnt, instr_r_valid,
    //Data interface
    input [31:0] data_read,
    output [31:0] data_write, data_adr,
    output data_req,
    input data_gnt, data_r_valid,
    output data_write_enable,
    output [3:0] data_be,
    //Interrupts
    input irq,
    input [4:0] irq_id,
    output irq_ack,
    output [4:0] irq_ack_id
    );
    
    // WIRE/REG DECLARATIONS
    //Controller
        wire pc_enable, instr_reg_enable, regset_write_enable;
    //Interrupt logic
        wire save_pc, irq_restore;
        wire irq_state, irq_load;
    //Datapath
        wire [31:0] regset_q0, regset_q1, alu_b, alu_out, imm_load, imm_upper_mux_out, instruction, pc_data, pc_out, result, imm_gen_out,alu_jalr_mux_out, alu_lui_mux_out;
        wire [31:0] pc_cond_jalr_mux_out, routine_irq_mux_out, pc_bak_out, mult_reg_to_mux, mult_mux_out, alu_csr_mux_out;
        wire mult_active, csr_active;
    //Instruction register logic
        wire [5:0] alu_op;
        wire store_logic, jalr_logic, pc_mode, regset_write_logic, alu_cmp;
    //Memory Controller
        wire data_req_intern, data_write_enable_intern, data_gnt_intern, data_r_valid_intern;
        wire [31:0] data_adr_intern, data_store_intern, data_load_intern;
    //Control & Status Registers
        wire[31:0] rdcycle_ls_out, rdcycle_ms_out, rdtime_ls_out, rdtime_ms_out, rdinstret_ls_out, rdinstret_ms_out;
        wire rdtime_res, rdinstret_inc;
        wire[11:0] csr;
        reg[31:0] csr_value;
    // ASSIGNMENTS
    //Interrupt logic
        assign irq_ack_id[4:0] = irq_id;
    // Datapath
        assign imm_load[31:0] = imm_upper_mux_out + imm_gen_out;//was missing bus width, possible error source
        assign instr_adr[31:0] = pc_out;
        assign mult_active = (instruction[6:0] == 7'b0110011) & instruction[25];
        assign csr_active = (instruction[6:0] == 7'b1110011) & (instruction[14:12] == 3'b010);
    //Instruction register logic
        assign alu_op[5] = instruction[30];
        assign alu_op[4:2] = instruction[14:12];
        assign alu_op[1:0] = instruction[6:5];
        assign store_logic = ~(instruction[6] | instruction[4]);
        assign jalr_logic = instruction[2] & ~instruction[3];
        assign pc_mode = (instruction[6] & (alu_cmp | instruction[2])) | irq_load | irq_restore; 
        assign regset_write_logic = (instruction[4] | instruction[2] | (~instruction[6] & ~instruction[4])) & regset_write_enable;
    //Memory Controller
        assign data_adr_intern[31:0] = regset_q0 + imm_gen_out;
        assign data_store_intern[31:0] = regset_q1;
    //Control & Status Registers
        assign rdtime_res = 0;
        assign csr = instruction[31:20];
        
        
    always @(csr)
    begin
        case(csr)
            `RDCYCLE_LS:    csr_value <= rdcycle_ls_out;
            `RDCYCLE_MS:    csr_value <= rdcycle_ms_out;
            `RDTIME_LS:     csr_value <= rdtime_ls_out;
            `RDTIME_MS:     csr_value <= rdtime_ms_out;
            `RDINSTRET_LS:  csr_value <= rdinstret_ls_out;
            `RDINSTRET_MS:  csr_value <= rdinstret_ms_out;
            `MHARTID_ADR:   csr_value <= MHARTID;
            default:        csr_value <= 32'b0;
        endcase
    end
    
    REG_DR_32 rdcycle_ls(.D(rdcycle_ls_out + 1),
        .Q(rdcycle_ls_out),
        .CLK(clk),
        .RES(res));
    REG_DR_32 rdcycle_ms(.D(rdcycle_ms_out + (rdcycle_ls_out == 32'hffff)),
        .Q(rdcycle_ms_out),
        .CLK(clk),
        .RES(res));
    REG_DR_32 rdtime_ls(.D(rdtime_ls_out + 1),
        .Q(rdtime_ls_out),
        .CLK(clk),
        .RES(res | rdtime_res));
    REG_DR_32 rdtime_ms(.D(rdtime_ms_out + (rdtime_ls_out == 32'hffff)),
        .Q(rdtime_ms_out),
        .CLK(clk),
        .RES(res | rdtime_res));
    REG_DR_32 rdinstret_ls(.D(rdinstret_ls_out + rdinstret_inc),
        .Q(rdinstret_ls_out),
        .CLK(clk),
        .RES(res));
    REG_DR_32 rdinstret_ms(.D(rdinstret_ms_out + ((rdinstret_ls_out == 32'hffff) & rdinstret_inc)),
        .Q(rdinstret_ms_out),
        .CLK(clk),
        .RES(res));
    MUX_2x1_32 alu_csr_mux(.I0(mult_mux_out),
        .I1(csr_value),
        .S(csr_active),
        .Y(alu_csr_mux_out));

    mealy_mem_acc_ctrl memctrl(.clk(clk),
        .res(res),
        .data_req_cpu(data_req_intern),
        .data_gnt_mem(data_gnt), 
        .data_r_valid_mem(data_r_valid),
        .write_enable_cpu(data_write_enable_intern),
        .func(instruction[14:12]),
        .adr_in(data_adr_intern), 
        .load_in(data_read), 
        .store_in(data_store_intern),
        .data_req_mem(data_req), 
        .data_gnt_cpu(data_gnt_intern), 
        .data_r_valid_cpu(data_r_valid_intern), 
        .write_enable_mem(data_write_enable),
        .data_be(data_be),
        .adr_out(data_adr), 
        .load_out(data_load_intern), 
        .store_out(data_write));
    ctrl controller(.clk(clk),
        .res(res),
        .instr_gnt(instr_gnt),
        .instr_r_valid(instr_r_valid),
        .data_gnt(data_gnt_intern),
        .data_r_valid(data_r_valid_intern),
        .data_access(store_logic),
        .store(instruction[5]),
        .mret((instruction[6:0] == 7'b1110011) & (instruction[14:12] == 3'b000)),
        .irq(irq),
        .irq_state(irq_state),
        .instr_req(instr_req),
        .pc_enable(pc_enable),
        .instr_reg_enable(instr_reg_enable),
        .regset_write_enable(regset_write_enable),
        .rdinstret_inc(rdinstret_inc),
        .mult_active(mult_active),
        .data_req(data_req_intern),
        .data_write_enable(data_write_enable_intern),
        .irq_restore(irq_restore),
        .irq_ack(irq_ack),
        .save_pc(save_pc),
        .irq_load(irq_load));
    pc pc(.D(pc_data),
        .MODE(pc_mode),
        .ENABLE(pc_enable),
        .RES(res),
        .CLK(clk),
        .PC_OUT(pc_out));
    REG_DRE_32 instr_reg(.D(instr_read),
        .Q(instruction),
        .CLK(clk),
        .RES(res),
        .ENABLE(instr_reg_enable));
    regset regset(.D(alu_csr_mux_out),
        .A_D(instruction[11:7]),
        .A_Q0(instruction[19:15]),
        .A_Q1(instruction[24:20]),
        .write_enable(regset_write_logic),
        .RES(res),
        .CLK(clk),
        .Q0(regset_q0),
        .Q1(regset_q1));
    MUX_2x1_32 alu_b_mux(.I0(imm_gen_out),
        .I1(regset_q1),
        .S(instruction[5]),
        .Y(alu_b));
    alu alu(.S(alu_op),
        .A(regset_q0),
        .B(alu_b),
        .mult_active(mult_active),
        .CMP(alu_cmp),
        .Q(alu_out));
    MUX_2x1_32 alu_lui_mux(.I0(alu_out),
        .I1(imm_load),
        .S(instruction[2]),
        .Y(alu_lui_mux_out));
    MUX_2x1_32 alu_jalr_mux(.I0(alu_lui_mux_out),
        .I1(pc_out + 4),
        .S(instruction[2] & instruction[6]),
        .Y(alu_jalr_mux_out));
    MUX_2x1_32 alui_store_mux(.I0(alu_jalr_mux_out),
        .I1(data_load_intern),
        .S(store_logic),
        .Y(result));
    MUX_2x1_32 pc_cond_jalr_mux(.I0(imm_gen_out + pc_out),
        .I1({data_adr[31:1], 1'b0}),
        .S(jalr_logic),
        .Y(pc_cond_jalr_mux_out));
    imm_gen imm_gen(.in(instruction[31:0]), .out(imm_gen_out));
    MUX_2x1_32 imm_upper_mux(.I0(pc_out),
        .I1(32'd0),
        .S(instruction[5]),
        .Y(imm_upper_mux_out));
    MUX_2x1_32 routine_irq_mux(.I0(pc_cond_jalr_mux_out),
        .I1(pc_bak_out),
        .S(irq_restore),
        .Y(routine_irq_mux_out)); 
    MUX_2x1_32 irq_next_mux(.I0(routine_irq_mux_out),
        .I1((irq_id << 2) + 32'h1c008000),
        .S(irq_load),
        .Y(pc_data));
    REG_DRE_32 pc_bak(.D(pc_out),
        .Q(pc_bak_out),
        .CLK(clk),
        .RES(res),
        .ENABLE(save_pc));
    RS_FF irq_state_ff(.R(irq_restore | res),
        .S(irq_ack),
        .CLK(clk),
        .Q(irq_state));
    REG_DR_32 mult_cache(.D(alu_out),
        .Q(mult_reg_to_mux),
        .CLK(clk),
        .RES(res));
    MUX_2x1_32 mult_mux(.I0(result),
        .I1(mult_reg_to_mux),
        .S(mult_active),
        .Y(mult_mux_out));
endmodule
