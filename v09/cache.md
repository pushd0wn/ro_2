
Abkürzungen
- c_ -> cached_instr
- i_ -> instr_

``` mermaid
stateDiagram-v2
    idle --> result: c_req && hit / c_gnt = 1 \n c_rvalid = 1
    idle --> miss: c_req && !hit / i_req = 1
    miss --> waiting: i_rvalid / i_req = 0 \n c_gnt = 1
    #miss --> reading: !i_rvalid && i_gnt /\n i_req = 0 \n c_gnt = 1 \n
    waiting --> result : ε / c_rvalid
    #reading --> result: i_rvalid / c_rvalid = 1
    result --> idle: ε / c_gnt = 0 \n c_rvalid = 0
```

