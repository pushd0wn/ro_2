module mealy_mem_hub(
    input clk, res,
    input [31:0] write_cpu_a, write_cpu_b, read_mem, adr_a, adr_b,
    input req_a, req_b, gnt_mem, r_valid_mem, write_enable_a, write_enable_b,
    input [3:0] be_a, be_b,
    output reg [31:0] write_mem, adr_mem,
    output [31:0] read_cpu,
    output reg [3:0] be_mem,
    output reg req_mem, gnt_a, gnt_b, r_valid_a, r_valid_b, write_enable_mem
);
    // direction from mem to cpu directly wired -> no need for case-dependent assignment
    // since cpu won't accept data as long as gnt_(cpu) is not set (case-dependent)
    assign read_cpu = read_mem;

    // STATEMACHINE
    // states
    localparam [2:0] S_idle = 3'd0, S_req_A = 2'd1, S_req_B = 2'd2, S_fin_A = 2'd3, S_fin_B = 3'd4;
    reg [2:0] state, next_state;

    // remember recent state due to fairness
    wire recent;
    RS_FF recent_ff(
        .R(state == S_req_A || res),
        .S(state == S_req_B),
        .CLK(clk),
        .Q(recent));
    wire valid;
    RS_FF valid_ff(
        .R(state == S_fin_A || state == S_fin_B || res),
        .S(r_valid_mem),
        .CLK(clk),
        .Q(valid));
    wire gnt;
    RS_FF gnt_ff(
        .R(state == S_fin_A || state == S_fin_B || res),
        .S(gnt_mem),
        .CLK(clk),
        .Q(gnt));

    always @(posedge clk, posedge res)
    begin
        state <= res ? S_idle : next_state;
    end

    always @(*)
    begin
        case (state)
            S_idle:
                if(recent && req_a)
                begin 
                    next_state <= S_req_A;
                    write_mem <= write_cpu_a;
                    adr_mem <= adr_a;
                    be_mem <= be_a;
                    req_mem <= 0;
                    gnt_a <= 0;
                    gnt_b <= 0;
                    r_valid_a <= 0;
                    r_valid_b <= 0;
                    write_enable_mem <= write_enable_a;
                end
                else if(req_b)
                begin
                    next_state <= S_req_B;
                    write_mem <= write_cpu_b;
                    adr_mem <= adr_b;
                    be_mem <= be_b;
                    req_mem <= 0;
                    gnt_a <= 0;
                    gnt_b <= 0;
                    r_valid_a <= 0;
                    r_valid_b <= 0;
                    write_enable_mem <= write_enable_b;
                end
                else if(req_a)
                begin
                    next_state <= S_req_A;
                    write_mem <= write_cpu_a;
                    adr_mem <= adr_a;
                    be_mem <= be_a;
                    req_mem <= 0;
                    gnt_a <= 0;
                    gnt_b <= 0;
                    r_valid_a <= 0;
                    r_valid_b <= 0;
                    write_enable_mem <= write_enable_a;
                end
                else
                begin
                    next_state <= S_idle;
                    write_mem <= 32'b0;
                    adr_mem <= 32'b0;
                    be_mem <= 4'b0;
                    req_mem <= 1'b0;
                    gnt_a <= 1'b0;
                    gnt_b <= 1'b0;
                    r_valid_a <= 1'b0;
                    r_valid_b <= 1'b0;
                    write_enable_mem <= 1'b0;
                end
            S_req_A:
                if ((!write_enable_a && valid) || (write_enable_a && gnt))
                begin
                    next_state <= S_fin_A;
                    write_mem <= write_cpu_a;
                    adr_mem <= adr_a;
                    be_mem <= be_a;
                    req_mem <= req_a;
                    gnt_a <= gnt_mem;
                    gnt_b <= 1'b0;
                    r_valid_a <= r_valid_mem;
                    r_valid_b <= 1'b0;
                    write_enable_mem <= 1'b0;
                end
                else
                begin
                    next_state <= S_req_A;
                    write_mem <= write_cpu_a;
                    adr_mem <= adr_a;
                    be_mem <= be_a;
                    req_mem <= req_a;
                    gnt_a <= gnt_mem;
                    gnt_b <= 1'b0;
                    r_valid_a <= r_valid_mem;
                    r_valid_b <= 1'b0;
                    write_enable_mem <= write_enable_a;
                end
            S_req_B:
                if ((!write_enable_b && valid) || (write_enable_b && gnt))
                begin
                    next_state <= S_fin_B;
                    write_mem <= write_cpu_b;
                    adr_mem <= adr_b;
                    be_mem <= be_b;
                    req_mem <= req_b;
                    gnt_a <= 1'b0;
                    gnt_b <= gnt_mem;
                    r_valid_a <= 1'b0;
                    r_valid_b <= r_valid_mem;
                    write_enable_mem <= 1'b0;
                end
                else
                begin
                    next_state <= S_req_B;
                    write_mem <= write_cpu_b;
                    adr_mem <= adr_b;
                    be_mem <= be_b;
                    req_mem <= req_b;
                    gnt_a <= 1'b0;
                    gnt_b <= gnt_mem;
                    r_valid_a <= 1'b0;
                    r_valid_b <= r_valid_mem;
                    write_enable_mem <= write_enable_b;
                end
            S_fin_A:
            begin
                next_state <= S_idle;
                write_mem <= write_cpu_a;
                adr_mem <= adr_a;
                be_mem <= be_a;
                req_mem <= 1'b0;
                gnt_a <= gnt_mem;
                gnt_b <= 1'b0;
                r_valid_a <= r_valid_mem;
                r_valid_b <= 1'b0;
                write_enable_mem <= 1'b0;
            end
            S_fin_B:
            begin
                next_state <= S_idle;
                write_mem <= write_cpu_b;
                adr_mem <= adr_b;
                be_mem <= be_b;
                req_mem <= 1'b0;
                gnt_a <= 1'b0;
                gnt_b <= gnt_mem;
                r_valid_a <= 1'b0;
                r_valid_b <= r_valid_mem;
                write_enable_mem <= 1'b0;
            end
            default:
            begin
                next_state <= S_idle;
                write_mem <= 32'b0;
                adr_mem <= 32'b0;
                be_mem <= 32'b0;
                req_mem <= 1'b0;
                gnt_a <= 1'b0;
                gnt_b <= 1'b0;
                r_valid_a <= 1'b0;
                r_valid_b <= 1'b0;
                write_enable_mem <= 1'b0;
            end
        endcase
    end
endmodule