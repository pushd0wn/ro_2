`timescale 1ns / 1ps
`define s0 3'b000
`define s1 3'b001
`define s2 3'b010
`define s3 3'b011
`define s4 3'b100


module ctrl(
    input clk, res,
    input instr_gnt, instr_r_valid,
    output reg instr_req, pc_enable, instr_reg_enable, regset_write_enable
    );
    reg [2:0] current_state;
    
    always @(posedge clk, posedge res)
    begin
        if(res==1)
        begin
            current_state <= `s0;
        end
        else
        begin
            case(current_state)
                `s0: current_state <= `s1;
                `s1: current_state <= `s2;
                `s2: current_state <= `s3;
                `s3: current_state <= (instr_gnt==1) ? `s4 : `s3;
                `s4: current_state <= (instr_r_valid==1) ? `s0 : `s4;
            endcase
        end
    end
    
    always @(current_state)
    begin
        case(current_state)
            `s0: begin
                regset_write_enable <= 0;
                instr_req <= 0;
                instr_reg_enable <= 0;
                pc_enable <= 0;
            end
            `s1: begin
                regset_write_enable <= 1;
                instr_req <= 0;
                instr_reg_enable <= 0;
                pc_enable <= 0;
            end
            `s2: begin
                regset_write_enable <= 0;
                instr_req <= 0;
                instr_reg_enable <= 0;
                pc_enable <= 1;
            end
            `s3: begin
                regset_write_enable <= 0;
                instr_req <= 1;
                instr_reg_enable <= 0;
                pc_enable <= 0;
            end
            `s4: begin
                regset_write_enable <= 0;
                instr_req <= 1;
                instr_reg_enable <= 1;
                pc_enable <= 0;
            end
            default: begin
                regset_write_enable <= 0;
                instr_req <= 0;
                instr_reg_enable <= 0;
                pc_enable <= 0;
            end
        endcase
    end    
endmodule