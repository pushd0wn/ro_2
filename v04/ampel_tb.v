//`timescale 1ns / 1ps
`define takt 1

module ampel_tb();
    reg CLK, RESN, BTN;
    wire[2:0] RGB;
    ampel dut(.BTN(BTN), .RESN(RESN), .CLK(CLK), .RGB(RGB));
    initial begin
        CLK = 0;
        RESN = 1;
        BTN = 0;
        #1  RESN = 0;
        @(negedge CLK) RESN = 1;
        @(negedge CLK) BTN = 1;
        @(negedge CLK) BTN = 0;
    end
    always
        #`takt CLK=~CLK;
endmodule
