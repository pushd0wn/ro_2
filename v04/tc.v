`define clk_per_second 100000000
`define second_to_tick 5
`define counter_width 32

module tc(
    input RES, CLK,
    output reg TICK
    );
    reg [`counter_width -1:0] counter_i;
    initial TICK = 0;
    always @(posedge CLK, posedge RES)
    begin
        if(RES)
            counter_i <= `counter_width'd0;
        else
        begin
            counter_i <= counter_i + 1;
            if(counter_i == (`second_to_tick * `clk_per_second))
            begin
                TICK <= 1;
                counter_i <= `counter_width'd0;
            end
            else
            begin
                TICK <= 0;
            end
        end
    end
endmodule
