addi a4, x0, 0

addi a0, x0, 1
beq a0, x0, error
addi a4, x0, 1

addi a0, x0, 2
add a1, a0, x0
slli a1, a1, 12
lui a2, 2
slt a3, a2, a1
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 3
auipc a2, 0
slti a1, a2, -1
slt a3, a1, x0
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 4
addi a2, x0, 42
sltiu a1, a2, 0xFF
beq a1, x0, error
addi a5, x0, 0
addi a6, x0, 0
slt a5, a1, x0
slt a6, x0, a1
slt a3, a5, a6
xori a3, a3, 1 
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 5
addi a2, x0, 42
xori a1, a2, 42
slt a3, a1, x0
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 6
ori a1, a2, 85
addi a2, x0, 0x7F
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 7
addi a2, x0, 42
andi a1, a2, 85
slt a3, a1, x0
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 8
srli a1, a0, 3
addi a2, x0, 1
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 9
addi a1, x0, -2
srai a1, a1, 1
addi a2, x0, -1
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 10
sub a1, a1, a2
slt a3, a1, x0
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 11
addi a1, x0, 2
addi a2, x0, 1
sll a1, a1, a2
addi a2, x0, 4
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 12
addi a1, x0, 2
addi a2, x0, 1
srl a1, a1, a2
addi a2, x0, 1
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 13
addi a1, x0, 1
addi a2, x0, 0b11111
sra a1, a1, a2
addi a2, x0, 0
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 14
addi a1, x0, 42
addi a2, x0, 7
slt a1, a1, a2
slt a3, a1, x0
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 15
sltu a1, x0, x0
slt a3, a1, x0
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 16
addi a1, x0, 42
addi a2, x0, 85
xor a1, a1, a2
addi a2, x0, 0x7F
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 17
addi a1, x0, 42
addi a2, x0, 85
or a1, a1, a2
addi a2, x0, 0x7F
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 18
addi a1, x0, -1
addi a2, x0, 0xF8
and a1, a1, a2
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 19
addi a7, x0, 69
sw a7, 0x1 (x0)
lw a5, 0x1 (x0)
slt a6, a7, a5
slli a4, a4, 1
add a4, a4, a6

addi a0, x0, 20
auipc a1, 0b
jalr a2, a1, 0x10
addi a3, x0, 2
jump_back:
jal x0, jump_jal
addi a3, a3, 1
jalr a1, a2, 0
addi a3, a3, 3
jump_jal:
addi a3, a3, 4
addi a2, x0, 7
slt a5, a2, a3
slli a4, a4, 1
add a4, a4, a5

addi a0, x0, 21
slli a4, a4, 1
bne a0, x0, bne_success
addi a4, a4, 1
bne_success:

addi a0, x0, 22
slli a4, a4, 1
blt x0, a0, blt_success
addi a4, a4, 1
blt_success:

addi a0, x0, 23
slli a4, a4, 1
bge a0, x0, bge_success
addi a4, a4, 1
bge_success:

addi a0, x0, 24
slli a4, a4, 1
addi a5, x0, -1
bltu a0, a5, bltu_success
addi a4, a4, 1
bltu_success:

addi a0, x0, 25
slli a4, a4, 1
bgeu a5, a0, bgeu_success
addi a4, a4, 1
bgeu_success:

lui a0, 0b10000000000000
beq a4, a0, error

nop
error:
nop