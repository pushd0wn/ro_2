module mealy_mem_acc_ctrl(
    input clk, res,
    input data_req_cpu, data_gnt_mem, data_r_valid_mem, write_enable_cpu,
    input [2:0] func,
    input [31:0] adr_in, load_in, store_in,
    output reg data_req_mem, data_gnt_cpu, data_r_valid_cpu, 
    output write_enable_mem,
    output reg [3:0] data_be,
    output [31:0] store_out, adr_out, load_out
);
    // states
    localparam [2:0]    S_loadA = 3'b000,
                        S_gntA = 3'b001,
                        S_loadB = 3'b010,
                        S_gntB = 3'b011,
                        S_WB = 3'b100,
                        S_idle = 3'b101,
                        S_interstate_A = 3'b110,
                        S_interstate_B = 3'b111;

    
    // intern registers to hold values for multiple access
    reg enable_A, enable_B;
    reg [2:0] state, next_state;

    wire [31:0] wire_A, wire_B;
    
    // logic for operation
    wire word, halfword, byte, halfword_u, byte_u;
    wire [4:0] operation;

    assign word = (func[2:0] == 3'b010);
    assign halfword = (func[2:0] == 3'b001);
    assign byte = (func[2:0] == 3'b000);
    assign halfword_u = (func[2:0] == 3'b101);
    assign byte_u = (func[2:0] == 3'b100);

    assign operation = {byte_u, halfword_u, byte, halfword, word};
    
    // intern shit
    wire sec_access;
    wire[3:0] be_splitted_A, be_splitted_B;
    wire [7:0] be_total;
    wire [1:0] offset;
    reg writing_B;
    wire write_enable_active;
        
    assign sec_access = (byte == 0 && offset[1:0]==2'b11) || (word == 1 && offset[1:0] != 2'b00);
    assign offset = adr_in[1:0];
    assign be_total = ((word) ? 8'b0000_1111 : ((halfword || halfword_u) ? 8'b0000_0011 : ((byte || byte_u) ? 8'b0000_0001 : 8'b0))) << (offset); // shift be to right position
    assign be_splitted_A = be_total[3:0];
    assign be_splitted_B = be_total[7:4];

    // going out right through the memory ~ freaky shit
    assign write_enable_mem = write_enable_cpu;
    assign store_out = offset[1] ? (offset[0] ? {store_in[7:0], store_in[31:8]} : {store_in[15:0], store_in[31:16]}) : (offset[0] ? {store_in[23:0], store_in[31:24]} : store_in[31:0]);
//    wire c, d;
//    assign c = store_in << (offset * 8);
//    assign d = store_in >> ((4 - offset) * 8);
//    assign store_out = c | d;

    assign adr_out = {(adr_in[31:2] + writing_B), 2'b0};
    
    // flipflop to hold state of write_enable_cpu during whole cycle
    RS_FF we_active(
        .R(state == S_idle),
        .S(write_enable_cpu),
        .CLK(clk),
        .Q(write_enable_active));

    REG_DRE_32 A(
        .D(load_in),
        .Q(wire_A),
        .CLK(clk),
        .RES(res),
        .ENABLE(enable_A));
    
    REG_DRE_32 B(
        .D(load_in),
        .Q(wire_B),
        .CLK(clk),
        .RES(res),
        .ENABLE(enable_B));

    table5x4_sel l_order(
        .col(adr_in[1:0]),
        .row(operation),
        .in_0x0(wire_A[31:0]),
        .in_0x1({wire_B[7:0],wire_A[31:8]}),
        .in_0x2({wire_B[15:0],wire_A[31:16]}),
        .in_0x3({wire_B[23:0],wire_A[31:24]}),
        .in_1x0({{16{wire_A[15]}},wire_A[15:0]}),
        .in_1x1({{16{wire_A[23]}},wire_A[23:8]}),
        .in_1x2({{16{wire_A[31]}},wire_A[31:16]}),
        .in_1x3({{16{wire_B[7]}},wire_B[7:0],wire_A[31:24]}),
        .in_2x0({{24{wire_A[7]}},wire_A[7:0]}),
        .in_2x1({{24{wire_A[15]}},wire_A[15:8]}),
        .in_2x2({{24{wire_A[23]}},wire_A[23:16]}),
        .in_2x3({{24{wire_A[31]}},wire_A[31:24]}),
        .in_3x0({16'b0,wire_A[15:0]}),
        .in_3x1({16'b0,wire_A[23:8]}),
        .in_3x2({16'b0,wire_A[31:16]}),
        .in_3x3({16'b0,wire_B[7:0],wire_A[31:24]}),
        .in_4x0({24'b0,wire_A[7:0]}),
        .in_4x1({24'b0,wire_A[15:8]}),
        .in_4x2({24'b0,wire_A[23:16]}),
        .in_4x3({24'b0,wire_A[31:24]}),
        .selected(load_out));

    always @(posedge clk, posedge res)
    begin
        state <= res ? S_loadA : next_state;
    end

    always @(*)
    begin
        case(state)
            S_idle:
            begin
                next_state <= S_loadA;
                data_req_mem <= 0;
                data_gnt_cpu <= 0;
                data_r_valid_cpu <= 0;
                data_be <= be_splitted_A;
                enable_A <= 0;
                enable_B <= 0;
                writing_B <= 0;
                // write_enable_mem <= write_enable_cpu;
             end
//                if(data_req_cpu)
//                begin
//                    next_state <= S_loadA;
//                    data_req_mem <= 1;
//                    data_gnt_cpu <= 0;
//                    data_r_valid_cpu <= 0;
//                    data_be <= be_splitted_A;
//                    enable_A <= !write_enable_cpu;
//                    enable_B <= 0;
//                    writing_B <= 0;
//                    // write_enable_mem <= write_enable_cpu;
//                end
//                else
//                begin
//                    next_state <= S_idle;
//                    data_req_mem <= 0;
//                    data_gnt_cpu <= 0;
//                    data_r_valid_cpu <= 0;
//                    data_be <= be_splitted_A;
//                    enable_A <= 0;
//                    enable_B <= 0;
//                    writing_B <= 0;
//                    // write_enable_mem <= 0;
//                end
//            end
            S_loadA:
            begin
                if (data_gnt_mem)
                begin
                    next_state <= S_interstate_A;
                    data_req_mem <= data_req_cpu;
                    data_gnt_cpu <= !sec_access;
                    data_r_valid_cpu <= 0;
                    data_be <= be_splitted_A;
                    enable_A <= !write_enable_active;
                    enable_B <= 0;
                    writing_B <= 0;
                    // write_enable_mem <= 0;
                end
                else
                begin
                    next_state <= S_loadA;
                    data_req_mem <= data_req_cpu;
                    data_gnt_cpu <= 0;
                    data_r_valid_cpu <= 0;
                    data_be <= be_splitted_A;
                    enable_A <= 0;
                    enable_B <= 0;
                    writing_B <= 0;
                    // write_enable_mem <= write_enable_cpu;
                end
            end
            S_interstate_A:
            begin
                if ((write_enable_active || data_r_valid_mem) && !sec_access)
                begin
                    next_state <= S_WB;
                    data_req_mem <= 0;
                    data_gnt_cpu <= 1;
                    data_r_valid_cpu <= 0;
                    data_be <= be_splitted_A;
                    enable_A <= !write_enable_active;
                    enable_B <= 0;
                    writing_B <= 0;
                    // write_enable_mem <= 0;
                end
                else if (sec_access && data_r_valid_mem)
                begin
                    next_state <= S_loadB;
                    data_req_mem <= 1;
                    data_gnt_cpu <= 0;
                    data_r_valid_cpu <= 0;
                    data_be <= be_splitted_B;
                    enable_A <= !write_enable_active;
                    enable_B <= !write_enable_active;
                    writing_B <= 1;
                    // write_enable_mem <= 0;
                end
                else 
                begin
                    next_state <= S_gntA;
                    data_req_mem <= 0;
                    data_gnt_cpu <= !sec_access;
                    data_r_valid_cpu <= 0;
                    data_be <= be_splitted_A;
                    enable_A <= !write_enable_active;
                    enable_B <= 0;
                    writing_B <= 0;
                    // write_enable_mem <= 0;
                end
            end
            S_gntA:
            begin
                if (sec_access)
                begin
                    if (write_enable_active || data_r_valid_mem)
                    begin
                        next_state <= S_loadB;
                        data_req_mem <= 1;
                        data_gnt_cpu <= 0;
                        data_r_valid_cpu <= 0;
                        data_be <= be_splitted_B;
                        enable_A <= 0;
                        enable_B <= !write_enable_active;
                        writing_B <= 1;
                        // write_enable_mem <= write_enable_cpu;
                    end
                    else
                    begin
                        next_state <= S_gntA;
                        data_req_mem <= 0;
                        data_gnt_cpu <= !sec_access;
                        data_r_valid_cpu <= 0;
                        data_be <= be_splitted_A;
                        enable_A <= !write_enable_active;
                        enable_B <= 0;
                        writing_B <= 0;
                        // write_enable_mem <= 0;
                    end
                end 
                else if (data_r_valid_mem)
                begin
                    next_state <= S_WB;
                    data_req_mem <= 0;
                    data_gnt_cpu <= 1;
                    data_r_valid_cpu <= 0;
                    data_be <= be_splitted_A;
                    enable_A <= 0;
                    enable_B <= 0;
                    writing_B <= 0;
                    // write_enable_mem <= 0;
                end
                else
                begin
                    next_state <= S_gntA;
                    data_req_mem <= 0;
                    data_gnt_cpu <= !sec_access;
                    data_r_valid_cpu <= 0;
                    data_be <= be_splitted_A;
                    enable_A <= !write_enable_active;
                    enable_B <= 0;
                    writing_B <= 0;
                    // write_enable_mem <= 0;
                end
            end
            S_loadB:
            begin
                if (data_gnt_mem)
                begin
                    next_state <= S_interstate_B;
                    data_req_mem <= 1;
                    data_gnt_cpu <= !sec_access;
                    data_r_valid_cpu <= 0;
                    data_be <= be_splitted_B;
                    enable_A <= 0;
                    enable_B <= !write_enable_active;
                    writing_B <= 1;
                    // write_enable_mem <= 0;   
                end
                else
                begin
                    next_state <= S_loadB;
                    data_req_mem <= 1;
                    data_gnt_cpu <= 0;
                    data_r_valid_cpu <= 0;
                    data_be <= be_splitted_B;
                    enable_A <= 0;
                    enable_B <= !write_enable_active;
                    writing_B <= 1;
                    // write_enable_mem <= write_enable_cpu;
                end
            end
            S_interstate_B:
            begin
                if (write_enable_active || data_r_valid_mem)
                begin
                    next_state <= S_WB;
                    data_req_mem <= 0;
                    data_gnt_cpu <= 1;
                    data_r_valid_cpu <= 0;
                    data_be <= be_splitted_B;
                    enable_A <= 0;
                    enable_B <= 0;
                    writing_B <= 1;
                    // write_enable_mem <= 0;
                end
                else
                begin
                    next_state <= S_gntB;
                    data_req_mem <= 0;
                    data_gnt_cpu <= 1;
                    data_r_valid_cpu <= 0;
                    data_be <= be_splitted_B;
                    enable_A <= 0;
                    enable_B <= !write_enable_active;
                    writing_B <= 1;
                    // write_enable_mem <= 0;
                end
            end
            S_gntB:
            begin
                if (data_r_valid_mem)
                begin
                    next_state <= S_WB;
                    data_req_mem <= 0;
                    data_gnt_cpu <= 0;
                    data_r_valid_cpu <= 0;
                    data_be <= be_splitted_B;
                    enable_A <= 0;
                    enable_B <= 0;
                    writing_B <= 1;
                    // write_enable_mem <= 0;
                end
                else
                begin
                    next_state <= S_gntB;
                    data_req_mem <= 0;
                    data_gnt_cpu <= 1;
                    data_r_valid_cpu <= 0;
                    data_be <= be_splitted_B;
                    enable_A <= 0;
                    enable_B <= !write_enable_active;
                    writing_B <= 1;
                    // write_enable_mem <= 0;
                end
            end
            S_WB:
            begin
                next_state <= S_idle;
                data_req_mem <= 0;
                data_gnt_cpu <= 0;
                data_r_valid_cpu <= 1;
                data_be <= be_splitted_A;
                enable_A <= 0;
                enable_B <= 0;
                writing_B <= 0;
                // write_enable_mem <= 0;
            end
            default:
            begin
                next_state <= S_loadA;
                data_req_mem <= 0;
                data_gnt_cpu <= 0;
                data_r_valid_cpu <= 0;
                data_be <= be_splitted_A;
                enable_A <= 0;
                enable_B <= 0;
                writing_B <= 0;
                // write_enable_mem <= 0;
            end
        endcase
    end
endmodule