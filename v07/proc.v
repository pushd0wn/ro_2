`timescale 1ns / 1ps

module proc(
    input clk, res,
    //Instruction memory interface
    input [31:0] instr_read,
    output [31:0] instr_adr,
    output instr_req,
    input instr_gnt, instr_r_valid,
    //Data interface
    input [31:0] data_read,
    output [31:0] data_write, data_adr,
    output data_req,
    input data_gnt, data_r_valid,
    output data_write_enable,
    output [3:0] data_be,
    //Interrupts
    input irq,
    input [4:0] irq_id,
    output irq_ack,
    output [4:0] irq_ack_id
    );
    assign data_be = 4'b1111;
    //Controller
    wire pc_enable, instr_reg_enable, regset_write_enable;
    //Interrupt logic
    wire save_pc, irq_restore;
    wire irq_state, irq_load;
    assign irq_ack_id[4:0] = irq_id;
    //Instruction register logic
    wire [5:0] alu_op;
    assign alu_op[5] = instruction[30];
    assign alu_op[4:2] = instruction[14:12];
    assign alu_op[1:0] = instruction[6:5];
    wire store_logic, jalr_logic, pc_mode, regset_write_logic, alu_cmp;
    assign store_logic = ~(instruction[6] | instruction[4]);
    assign jalr_logic = instruction[2] & ~instruction[3];
    assign pc_mode = (instruction[6] & (alu_cmp | instruction[2])) | irq_load | irq_restore; 
    assign regset_write_logic = (instruction[4] | instruction[2] | (~instruction[6] & ~instruction[4])) & regset_write_enable;
    //Datapath
    wire [31:0] regset_q0, regset_q1, alu_b, alu_out, imm_load, imm_upper_mux_out, instruction, pc_data, pc_out, result, imm_gen_out,alu_jalr_mux_out, alu_lui_mux_out;
    wire [31:0] pc_cond_jalr_mux_out, routine_irq_mux_out, pc_bak_out;
    assign data_adr[31:0] = regset_q0 + imm_gen_out;
    assign data_write[31:0] = regset_q1;
    assign imm_load[31:0] = imm_upper_mux_out + imm_gen_out;//was missing bus width, possible error source
    assign instr_adr[31:0] = pc_out;

    ctrl controller(.clk(clk),
        .res(res),
        .instr_gnt(instr_gnt),
        .instr_r_valid(instr_r_valid),
        .data_gnt(data_gnt),
        .data_r_valid(data_r_valid),
        .data_access(store_logic),
        .store(instruction[5]),
        .mret(instruction[4] & instruction[6]),
        .irq(irq),
        .irq_state(irq_state),
        .instr_req(instr_req),
        .pc_enable(pc_enable),
        .instr_reg_enable(instr_reg_enable),
        .regset_write_enable(regset_write_enable),
        .data_req(data_req),
        .data_write_enable(data_write_enable),
        .irq_restore(irq_restore),
        .irq_ack(irq_ack),
        .save_pc(save_pc),
        .irq_load(irq_load));
    pc pc(.D(pc_data),
        .MODE(pc_mode),
        .ENABLE(pc_enable),
        .RES(res),
        .CLK(clk),
        .PC_OUT(pc_out));
    REG_DRE_32 instr_reg(.D(instr_read),
        .Q(instruction),
        .CLK(clk),
        .RES(res),
        .ENABLE(instr_reg_enable));
    regset regset(.D(result),
        .A_D(instruction[11:7]),
        .A_Q0(instruction[19:15]),
        .A_Q1(instruction[24:20]),
        .write_enable(regset_write_logic),
        .RES(res),
        .CLK(clk),
        .Q0(regset_q0),
        .Q1(regset_q1));
    MUX_2x1_32 alu_b_mux(.I0(imm_gen_out),
        .I1(regset_q1),
        .S(instruction[5]),
        .Y(alu_b));
    alu alu(.S(alu_op),
        .A(regset_q0),
        .B(alu_b),
        .CMP(alu_cmp),
        .Q(alu_out));
    MUX_2x1_32 alu_lui_mux(.I0(alu_out),
        .I1(imm_load),
        .S(instruction[2]),
        .Y(alu_lui_mux_out));
    MUX_2x1_32 alu_jalr_mux(.I0(alu_lui_mux_out),
        .I1(pc_out + 4),
        .S(instruction[2] & instruction[6]),
        .Y(alu_jalr_mux_out));
    MUX_2x1_32 alui_store_mux(.I0(alu_jalr_mux_out),
        .I1(data_read),
        .S(store_logic),
        .Y(result));
    MUX_2x1_32 pc_cond_jalr_mux(.I0(imm_gen_out + pc_out),
        .I1({data_adr[31:1], 1'b0}),
        .S(jalr_logic),
        .Y(pc_cond_jalr_mux_out));
    imm_gen imm_gen(.in(instruction[31:0]), .out(imm_gen_out));
    MUX_2x1_32 imm_upper_mux(.I0(pc_out),
        .I1(32'd0),
        .S(instruction[5]),
        .Y(imm_upper_mux_out));
    MUX_2x1_32 routine_irq_mux(.I0(pc_cond_jalr_mux_out),
        .I1(pc_bak_out),
        .S(irq_restore),
        .Y(routine_irq_mux_out)); 
    MUX_2x1_32 irq_next_mux(.I0(routine_irq_mux_out),
    .I1((irq_id << 2) + 32'h1c008000),
    .S(irq_load),
    .Y(pc_data));
    REG_DRE_32 pc_bak(.D(pc_out),
        .Q(pc_bak_out),
        .CLK(clk),
        .RES(res),
        .ENABLE(save_pc));
    RS_FF irq_state_ff(.R(irq_restore | res),
        .S(irq_ack),
        .CLK(clk),
        .Q(irq_state));
endmodule
