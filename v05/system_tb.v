`timescale 1ns / 1ps

module system_tb();
    reg clk, res;
    wire instr_req, instr_gnt, instr_r_valid, data_req, data_gnt, data_r_valid, data_write_enable, irq, irq_ack;
    wire [31:0] instr_read, instr_adr, data_read, data_write, data_adr;
    wire [4:0] irq_id, irq_ack_id;
    wire [3:0] data_be;
    
    proc cpu(.clk(clk), .res(res),
        .instr_read(instr_read),
        .instr_adr(instr_adr),
        .instr_req(instr_req),
        .instr_gnt(instr_gnt),
        .instr_r_valid(instr_r_valid),
        .data_read(data_read),
        .data_write(data_write),
        .data_adr(data_adr),
        .data_req(data_req),
        .data_gnt(data_gnt),
        .data_r_valid(data_r_valid),
        .data_write_enable(data_write_enable),
        .data_be(data_be),
        .irq(irq),
        .irq_id(irq_id),
        .irq_ack(irq_ack),
        .irq_ack_id(irq_ack_id));
    memory_sim memory(.clk_i(clk),
        .data_read(instr_read),
        .data_write(0),
        .data_adr(instr_adr),
        .data_req(instr_req),
        .data_gnt(instr_gnt),
        .data_rvalid(instr_r_valid),
        .data_write_enable(0));
    initial begin
        clk <= 0;
        res <= 1;
        @(posedge clk)@(posedge clk)
        res <= 0;
    end
    
    always
        #20 clk = ~clk;
endmodule
