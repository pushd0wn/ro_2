`include "v_lib/riscv_isa_defines.v"
module imm_gen(
    input [31:0] in,
    output reg [31:0] out
);
    always @(in)
    begin
        casez(in[6:0])
            `OPCODE_OP: out = 32'b0;  
            `OPCODE_OPIMM: out = (3'b101 == in[14:12]) ? 
                {27'b0, in[24:20]} : ((0 == in[31]) ? {20'b0, in[31:20]} :
                {{20{1'b1}}, in[31:20]});
            `OPCODE_STORE: out = (0 == in[31]) ?
                {20'b0, in[31:25], in[11:7]} : {{20{1'b1}}, in[31:25], in[11:7]};
            `OPCODE_LOAD: out = (0 == in[31]) ?
                {20'b0, in[31:20]} : {{20{1'b1}}, in[31:20]};
            `OPCODE_BRANCH: out = (0 == in[31]) ?
                {19'b0, in[31], in[7], in[30:25], in[11:8], 1'b0} :
                {{19{1'b1}}, in[31], in[7], in[30:25], in[11:8], 1'b0};
            `OPCODE_JALR: out = (0 == in[31]) ?
                {20'b0, in[31:20]} : {{20{1'b1}}, in[31:20]};
            `OPCODE_JAL: out = (0 == in[31]) ?
                {11'b0, in[31], in[19:12], in[20], in[30:21], 1'b0} :
                {{11{1'b1}}, in[31], in[19:12], in[20], in[30:21], 1'b0};
            `OPCODE_AUIPC: out = {in[31:12], 12'b0};
            `OPCODE_LUI: out = {in[31:12], 12'b0};
            default: out = 32'b0;
        endcase
    end
        
    

endmodule