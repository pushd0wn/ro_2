module alu_tb();
    reg [31:0] A, B;
    reg [5:0] S;
    wire [31:0] Q;
    wire CMP;
    reg [31:0] soll;
    // ALU als Design-Under-Test (DUT) instanziieren
    alu dut(.A(A), .B(B), .S(S), .Q(Q), .CMP(CMP));

    // Initial Block mit allen Testcases
    initial begin
        // BEQ
        A=0; B=0; S=6'b100011; soll=1; #10;
        if( CMP != soll)
        begin
            $display("Testmuster 1 Fehlgeschlagen: ist=%h soll=%h", CMP, soll);
            $finish;
        end
        // BEQ
        A=1; B=0; S=6'b100011; soll=0; #10;
        if( CMP != soll)
        begin
            $display("Testmuster 2 Fehlgeschlagen: ist=%h soll=%h", CMP, soll);
            $finish;
        end
        // BNE
        A=0; B=0; S=6'b100111; soll=0; #10;
        if( CMP != soll)
        begin
            $display("Testmuster 3 Fehlgeschlagen: ist=%h soll=%h", CMP, soll);
            $finish;
        end
        // BNE
        A=1; B=0; S=6'b100111; soll=1; #10;
        if( CMP != soll)
        begin
            $display("Testmuster 4 Fehlgeschlagen: ist=%h soll=%h", CMP, soll);
            $finish;
        end
        // BLT
        A=42; B=7; S=6'b010011; soll=0; #10;
        if( CMP != soll)
        begin
            $display("Testmuster 5 Fehlgeschlagen: ist=%h soll=%h", CMP, soll);
            $finish;
        end
        // BLT
        A=5; B=23; S=6'b010011; soll=1; #10;
        if( CMP != soll)
        begin
            $display("Testmuster 6 Fehlgeschlagen: ist=%h soll=%h", CMP, soll);
            $finish;
        end
        // BGE
        A=-17; B=-17; S=6'b010111; soll=1; #10;
        if( CMP != soll)
        begin
            $display("Testmuster 7 Fehlgeschlagen: ist=%h soll=%h", CMP, soll);
            $finish;
        end
        // BGE
        A=-100000; B=65536; S=6'b010111; soll=0; #10;
        if( CMP != soll)
        begin
            $display("Testmuster 8 Fehlgeschlagen: ist=%h soll=%h", CMP, soll);
            $finish;
        end
        // BLTU
        A=0; B=0; S=6'b111011; soll=0; #10;
        if( CMP != soll)
        begin
            $display("Testmuster 9 Fehlgeschlagen: ist=%h soll=%h", CMP, soll);
            $finish;
        end
        // BLTU
        A=0; B=2**31; S=6'b011011; soll=1; #10;
        if( CMP != soll)
        begin
            $display("Testmuster 10 Fehlgeschlagen: ist=%h soll=%h", CMP, soll);
            $finish;
        end
        // BGEU
        A=2**31; B=10; S=6'b111111; soll=1; #10;
        if( CMP != soll)
        begin
            $display("Testmuster 11 Fehlgeschlagen: ist=%h soll=%h", CMP, soll);
            $finish;
        end
        // BGEU
        A=2**31; B=(2**31)+1; S=6'b111111; soll=0; #10;
        if( CMP != soll)
        begin
            $display("Testmuster 12 Fehlgeschlagen: ist=%h soll=%h", CMP, soll);
            $finish;
        end
        // ADD
        A=5; B=7; S=6'b000001; soll=12; #10;
        if( Q != soll)
        begin
            $display("Testmuster 13 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // ADDI
        A=-3; B=5; S=6'b000000; soll=2; #10;
        if( Q != soll)
        begin
            $display("Testmuster 14 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // SUB
        A=-3; B=-5; S=6'b100001; soll=2; #10;
        if( Q != soll)
        begin
            $display("Testmuster 15 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // SUB
        A=3; B=5; S=6'b100001; soll=-2; #10;
        if( Q != soll)
        begin
            $display("Testmuster 16 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // SLL
        A=2; B=1; S=6'b000101; soll=4; #10;
        if( Q != soll)
        begin
            $display("Testmuster 17 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // SLLI
        A=-1; B=1; S=6'b000100; soll=-2; #10;
        if( Q != soll)
        begin
            $display("Testmuster 18 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // SLT
        A=42; B=7; S=6'b001001; soll=0; #10;
        if( Q != soll)
        begin
            $display("Testmuster 19 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // SLTI
        A=5; B=23; S=6'b001000; soll=1; #10;
        if( Q != soll)
        begin
            $display("Testmuster 20 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // SLTU
        A=0; B=0; S=6'b001101; soll=0; #10;
        if( Q != soll)
        begin
            $display("Testmuster 21 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // SLTIU
        A=32'hFFE; B=32'hFFF; S=6'b101100; soll=1; #10;
        if( Q != soll)
        begin
            $display("Testmuster 22 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // SRL
        A=-1; B=1; S=6'b010101; soll=(2**31)-1; #10;
        if( Q != soll)
        begin
            $display("Testmuster 23 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // SRLI
        A=-1; B=(2**5)-1; S=6'b010100; soll=1; #10;
        if( Q != soll)
        begin
            $display("Testmuster 24 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // SRA
        A=1; B=(2**5)-1; S=6'b110101; soll=0; #10;
        if( Q != soll)
        begin
            $display("Testmuster 25 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // SRAI
        A=-2; B=1; S=6'b110100; soll=-1; #10;
        if( Q != soll)
        begin
            $display("Testmuster 26 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // AND
        A=42; B=85; S=6'b011101; soll=0; #10;
        if( Q != soll)
        begin
            $display("Testmuster 27 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // ANDI
        A=32'h3F; B=32'hF8; S=6'b011100; soll=32'h38; #10;
        if( Q != soll)
        begin
            $display("Testmuster 28 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // OR
        A=42; B=85; S=6'b011001; soll=32'h7F; #10;
        if( Q != soll)
        begin
            $display("Testmuster 29 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // ORI
        A=0; B=0; S=6'b011000; soll=0; #10;
        if( Q != soll)
        begin
            $display("Testmuster 30 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // XOR
        A=42; B=85; S=6'b010001; soll=32'h7F; #10;
        if( Q != soll)
        begin
            $display("Testmuster 31 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
        // XORI
        A=42; B=32'h7F; S=6'b010000; soll=85; #10;
        if( Q != soll)
        begin
            $display("Testmuster 32 Fehlgeschlagen: ist=%h soll=%h", Q, soll);
            $finish;
        end
    end
endmodule