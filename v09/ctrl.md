| State         | write_enable | instr_req | instr_reg_enable | pc_enable | data_req | data_write_enable | save_pc | irq_ack | irq_load | irq_restore |
|-              | -            | -         | -                | -         | -        | -                 | -       | -       | -        | -           |
| begin_routine | 0            | 0     | **0**            | 0         | 0        | 0                 | 0       | 0       | 0        | 0           |
| load_data     | **1**        | 0         | 0                | 0         | **1**    | 0                 | 0       | 0       | 0        | 0           |
| write_data    | 0            | 0         | 0                | 0         | **1**    | **1**             | 0       | 0       | 0        | 0           |
| write_result  | **1**        | 0         | 0                | 0         | 0        | 0                 | 0       | 0       | 0        | 0           |
| update_pc     | **0**        | 0         | 0                | **1**     | **0**    | **0**             | 0       | 0       | 0        | 0           |
| save_pc       | 0            | 0         | 0                | **0**     | 0        | 0                 | **1**   | 0       | 0        | 0           |
| ld_ir_addr    | 0            | 0         | 0                | **1**     | 0        | 0                 | **0**   | 0       | **1**    | 0           |
| ir_ack        | 0            | 0         | 0                | **0**     | 0        | 0                 | 0       | **1**   | **0**    | 0           |
| mret          | 0            | 0         | 0                | **1**     | 0        | 0                 | 0       | 0       | 0        | **1**       |
| instr_fetch   | 0            | **1**     | 0                | **0**     | 0        | 0                 | 0       | **0**   | 0        | **0**       |
| instr_decode  | 0            | **0**     | **1**            | 0         | 0        | 0                 | 0       | 0       | 0        | 0           |


``` mermaid
stateDiagram-v2
    begin_routine --> write_result : mret == 0 \n && data_access == 0
    begin_routine--> load_data : mret == 0 \n && data_access == 1 \n && store == 0
    begin_routine--> write_data : mret == 0 \n && data_access == 1 \n && store == 1
    begin_routine --> ld_ir_addr : mret == 1 \n && irq == 1
    begin_routine--> mret : mret==1 \n&& irq == 0
    load_data --> update_pc : data_r_valid == 1
    write_data --> update_pc : data_gnt == 1
    write_result --> update_pc 
    update_pc --> instr_fetch : irq == 0 \n|| irq_state == 1
    update_pc --> save_pc : irq == 1 \n && irq_state == 0
    save_pc --> ld_ir_addr
    ld_ir_addr --> ir_ack
    mret --> instr_fetch
    ir_ack --> instr_fetch
    instr_fetch --> begin_routine : instr_r_valid == 1
    instr_fetch --> instr_decode : instr_r_valid == 0 \n && instr_gnt == 1
    instr_decode --> begin_routine: instr_r_valid == 1
```