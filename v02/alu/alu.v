`define ALU_SUB 6'b100001
`define ALU_ADD 6'b?0000?
`define ALU_SRL 6'b01010?
`define ALU_SRA 6'b11010?

`define ALU_SLL 6'b00010?
`define ALU_SLT 6'b?0100?
`define ALU_SLTU 6'b?0110?
`define ALU_XOR 6'b?1000?
`define ALU_OR 6'b?1100?
`define ALU_AND 6'b?1110?


`define ALU_EQ 6'b?00011
`define ALU_NE 6'b?00111
`define ALU_LT 6'b?10011
`define ALU_GE 6'b?10111
`define ALU_LTU 6'b?11011
`define ALU_GEU 6'b?11111

module alu(
    input [5:0] S,
    input [31:0] A,
    input [31:0] B,
    output reg CMP,
    output reg [31:0] Q
    );
    always @(S, A, B)
    begin
        Q=32'd0;
        CMP=0;
        casez(S)
            `ALU_SUB: Q = A - B;
            `ALU_ADD: Q = A + B;
            `ALU_SRL: Q = A >> B;
            `ALU_SRA: Q = $signed(A) >>> $signed(B);
            `ALU_SLL: Q = A << B;
            `ALU_SLT: Q = $signed(A) < $signed(B) ? 1: 0;
            `ALU_SLTU: Q = A < B ? 1 : 0;
            `ALU_XOR: Q = A ^ B;
            `ALU_OR: Q = A | B;
            `ALU_AND: Q = A & B;
            `ALU_EQ: CMP = A == B ? 1 : 0;
            `ALU_NE: CMP = A != B ? 1 : 0;
            `ALU_LT: CMP = $signed(A) < $signed(B) ? 1: 0;
            `ALU_GE: CMP = $signed(A) >= $signed(B) ? 1: 0;
            `ALU_LTU: CMP = A < B ? 1 : 0;
            `ALU_GEU: CMP = A >= B ? 1: 0;
            default: begin
                Q=32'd0;
                CMP=0;
            end
        endcase
    end
endmodule