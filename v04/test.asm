addi a4, x0, 0

addi a0, x0, 1
beq a0 x0, error
addi a4, x0, 1

addi a0, x0, 2
add a1, a0, x0
slli a1, a1, 12
lui a2, 2
slt a3, a2, a1
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 3
auipc a2, 0
slti a1, a2, -1
slt a3, a1, x0
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 4
sltiu a1, a2, 0xFF
beq a1, x0, error
addi a5, x0, 0
addi a6, x0, 0
slt a5, a1, x0
slt a6, x0, a1
slt a3, a5, a6
xori a3, a3, 1 
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 5
addi a2, x0, 42
xori a1, a2, 42
slt a3, a1, x0
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 6
ori a1, a2, 85
addi a2, x0, 0x7F
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 7
addi a2, x0, 42
andi a1, a2, 85
slt a3, a1, x0
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 8
srli a1, a0, 3
addi a2, x0, 1
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 9
addi a1, x0, -2
srai a1, a1, 1
addi a2, x0, -1
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 10
sub a1, a1, a2
slt a3, a1, x0
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 11
addi a1, x0, 2
addi a2, x0, 1
sll a1, a1, a2
addi a2, x0, 4
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 12
addi a1, x0, 2
addi a2, x0, 1
srl a1, a1, a2
addi a2, x0, 1
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 13
addi a1, x0, 1
addi a2, x0, 0b11111
sra a1, a1, a2
addi a2, x0, 0
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 14
addi a1, x0, 42
addi a2, x0, 7
slt a1, a1, a2
slt a3, a1, x0
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 15
sltu a1, x0, x0
slt a3, a1, x0
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 16
addi a1, x0, 42
addi a2, x0, 85
xor a1, a1, a2
addi a2, x0, 0x7F
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 17
addi a1, x0, 42
addi a2, x0, 85
or a1, a1, a2
addi a2, x0, 0x7F
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

addi a0, x0, 18
addi a1, x0, -1
addi a2, x0, 0xF8
and a1, a1, a2
slt a3, a1, a2
slli a4, a4, 1
add a4, a4, a3

lui a0, 0b100000
beq a4, a0, error

nop
error:
nop
